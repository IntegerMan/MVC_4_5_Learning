﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MVC_4_5_Learning.Models
{
  public class Battlebot
  {
    [Key]
    public int Id { get; set; }

    public string Name { get; set; }

    [Display(Name="# Matches")]
    public int NumMatches { get; set; }
    [Display(Name = "# Wins")]
    public int NumWins { get; set; }
    [Display(Name = "# Losses")]
    public int NumLosses { get; set; }

    [Display(Name = "# Wheels")]
    public int NumWheels { get; set; }
    [Display(Name = "# Blades")]
    public int NumBlades { get; set; }
    [Display(Name = "Has Flamethrower?")]
    public bool HasFlamethrower { get; set; }

  }
}