﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MVC_4_5_Learning.Models;

namespace MVC_4_5_Learning.Repositories
{
  public interface IBattlebotRepository
  {
    Task AddAsync(Battlebot battlebot);
    Task DeleteAsync(int id);
    Task EditAsync(Battlebot battlebot);
    Task<Battlebot> GetAsync(int? id);
    Task<IEnumerable<Battlebot>> GetListAsync();
  }
}