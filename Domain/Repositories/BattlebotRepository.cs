﻿using System.Collections;
using System.Data.Entity;
using System.Threading.Tasks;
using MVC_4_5_Learning.Models;

namespace MVC_4_5_Learning.Repositories
{
  public class BattlebotRepository : IBattlebotRepository
  {

    private readonly AppContext _context;

    public BattlebotRepository()
    {
      _context = new AppContext();
    }

    public async Task<System.Collections.Generic.IEnumerable<Battlebot>> GetListAsync()
    {
      return await _context.Battlebots.ToListAsync();
    }

    public Task<Battlebot> GetAsync(int? id)
    {
      return _context.Battlebots.FindAsync(id);
    }

    public Task AddAsync(Battlebot battlebot)
    {
      _context.Battlebots.Add(battlebot);
      return _context.SaveChangesAsync();
    }

    public Task DeleteAsync(int id)
    {
      Battlebot battlebot = _context.Battlebots.Find(id);
      if (battlebot != null)
      {
        _context.Battlebots.Remove(battlebot);
        return _context.SaveChangesAsync();
      }
      else
      {
        return Task.FromResult(false);
      }

    }

    public Task EditAsync(Battlebot battlebot)
    {
      _context.Entry(battlebot).State = EntityState.Modified;
      return _context.SaveChangesAsync();
    }
  }

}