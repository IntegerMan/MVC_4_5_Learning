﻿using System.Collections;
using System.Data.Entity;
using System.Threading.Tasks;
using MVC_4_5_Learning.Models;

namespace MVC_4_5_Learning.Repositories
{
  public class BattlebotRepository : IBattlebotRepository
  {

    private readonly AppContext db;

    public BattlebotRepository()
    {
      db = new AppContext();
    }

    public async Task<System.Collections.Generic.IEnumerable<Battlebot>> GetListAsync()
    {
      return await db.Battlebots.ToListAsync();
    }

    public Task<Battlebot> GetAsync(int? id)
    {
      return db.Battlebots.FindAsync(id);
    }

    public Task AddAsync(Battlebot battlebot)
    {
      db.Battlebots.Add(battlebot);
      return db.SaveChangesAsync();
    }

    public Task DeleteAsync(int id)
    {
      Battlebot battlebot = db.Battlebots.Find(id);
      if (battlebot != null)
      {
        db.Battlebots.Remove(battlebot);
        return db.SaveChangesAsync();
      }
      else
      {
        return Task.FromResult(false);
      }

    }

    public Task EditAsync(Battlebot battlebot)
    {
      db.Entry(battlebot).State = EntityState.Modified;
      return db.SaveChangesAsync();
    }
  }

}