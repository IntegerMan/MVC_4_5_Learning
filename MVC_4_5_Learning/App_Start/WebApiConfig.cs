﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Mvc;
using MVC_4_5_Learning.Repositories;
using Unity;
using Unity.Mvc5;

namespace MVC_4_5_Learning
{
  public static class WebApiConfig
  {
    public static void Register(HttpConfiguration config)
    {
      // Set up Unity
      UnityConfig.RegisterComponents();

      // Web API configuration and services

      // Web API routes
      config.MapHttpAttributeRoutes();

      config.Routes.MapHttpRoute(
          name: "DefaultApi",
          routeTemplate: "api/{controller}/{id}",
          defaults: new { id = RouteParameter.Optional }
      );
    }
  }
}
