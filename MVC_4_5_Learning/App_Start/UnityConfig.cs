using MVC_4_5_Learning.Repositories;
using System.Web.Mvc;
using Unity;
using Unity.Mvc5;

namespace MVC_4_5_Learning
{
  public static class UnityConfig
  {
    public static void RegisterComponents()
    {
      var container = new UnityContainer();

      // register all your components with the container here
      // it is NOT necessary to register your controllers

      // e.g. container.RegisterType<ITestService, TestService>();
      container.RegisterType<IBattlebotRepository, BattlebotRepository>();

      DependencyResolver.SetResolver(new UnityDependencyResolver(container));
    }
  }
}