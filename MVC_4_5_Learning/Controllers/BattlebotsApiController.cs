﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using MVC_4_5_Learning.Models;
using MVC_4_5_Learning.Repositories;

namespace MVC_4_5_Learning.Controllers
{
  public class BattlebotsApiController : ApiController
  {
    private readonly IBattlebotRepository _repository;

    public BattlebotsApiController()
    {
      // TODO: Ideally, this should use Unity as well for dependency resolving
      _repository = new BattlebotRepository();
    }

    // GET: api/BattlebotsApi
    public async Task<IEnumerable<Battlebot>> GetBattlebots()
    {
      return await _repository.GetListAsync();
    }

    // GET: api/BattlebotsApi/5
    [ResponseType(typeof(Battlebot))]
    public async Task<IHttpActionResult> GetBattlebot(int id)
    {
      Battlebot battlebot = await _repository.GetAsync(id);
      if (battlebot == null)
      {
        return NotFound();
      }

      return Ok(battlebot);
    }

    // PUT: api/BattlebotsApi/5
    [ResponseType(typeof(void))]
    public async Task<IHttpActionResult> PutBattlebot(int id, Battlebot battlebot)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      if (id != battlebot.Id)
      {
        return BadRequest();
      }

      await _repository.EditAsync(battlebot);

      return StatusCode(HttpStatusCode.NoContent);
    }

    // POST: api/BattlebotsApi
    [ResponseType(typeof(Battlebot))]
    public async Task<IHttpActionResult> PostBattlebot(Battlebot battlebot)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState);
      }

      await _repository.AddAsync(battlebot);

      return CreatedAtRoute("DefaultApi", new { id = battlebot.Id }, battlebot);
    }

    // DELETE: api/BattlebotsApi/5
    [ResponseType(typeof(Battlebot))]
    public async Task<IHttpActionResult> DeleteBattlebot(int id)
    {

      await _repository.DeleteAsync(id);

      return Ok();
    }

  }
}