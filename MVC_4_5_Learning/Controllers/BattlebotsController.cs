﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;
using System.Web.Mvc;
using MVC_4_5_Learning.Helpers;
using MVC_4_5_Learning.Models;

namespace MVC_4_5_Learning.Controllers
{
  public class BattlebotsController : Controller
  {

    // GET: Battlebots
    public async Task<ActionResult> Index()
    {
      return View(await HttpClientHelpers.GetHttpGetData<IEnumerable<Battlebot>>("api/battlebotsApi"));
    }

    // GET: Battlebots/Details/5
    public async Task<ActionResult> Details(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      Battlebot battlebot = await HttpClientHelpers.GetHttpGetData<Battlebot>($"api/battlebotsApi/{id}");
      if (battlebot == null)
      {
        return HttpNotFound();
      }
      return View(battlebot);
    }

    // GET: Battlebots/Create
    public ActionResult Create()
    {
      return View();
    }

    // POST: Battlebots/Create
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Create([Bind(Include = "Id,Name,NumMatches,NumWins,NumLosses,NumWheels,NumBlades,HasFlamethrower")] Battlebot battlebot)
    {
      if (ModelState.IsValid)
      {
        await HttpClientHelpers.GetHttpPostResult("api/battlebotsApi", battlebot);
        return RedirectToAction("Index");
      }

      return View(battlebot);
    }

    // GET: Battlebots/Edit/5
    public async Task<ActionResult> Edit(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      Battlebot battlebot = await HttpClientHelpers.GetHttpGetData<Battlebot>($"api/battlebotsApi/{id}");
      if (battlebot == null)
      {
        return HttpNotFound();
      }
      return View(battlebot);
    }

    // POST: Battlebots/Edit/5
    // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
    // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> Edit([Bind(Include = "Id,Name,NumMatches,NumWins,NumLosses,NumWheels,NumBlades,HasFlamethrower")] Battlebot battlebot)
    {
      if (ModelState.IsValid)
      {
        await HttpClientHelpers.GetHttpPutResult($"api/battlebotsApi/{battlebot.Id}", battlebot);

        return RedirectToAction("Index");
      }
      return View(battlebot);
    }

    // GET: Battlebots/Delete/5
    public async Task<ActionResult> Delete(int? id)
    {
      if (id == null)
      {
        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
      }
      Battlebot battlebot = await HttpClientHelpers.GetHttpGetData<Battlebot>($"api/battlebotsApi/{id}");
      if (battlebot == null)
      {
        return HttpNotFound();
      }
      return View(battlebot);
    }

    // POST: Battlebots/Delete/5
    [HttpPost, ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<ActionResult> DeleteConfirmed(int id)
    {
      await HttpClientHelpers.GetHttpDeleteResult($"api/battlebotsApi/{id}");
      return RedirectToAction("Index");
    }

  }
}
