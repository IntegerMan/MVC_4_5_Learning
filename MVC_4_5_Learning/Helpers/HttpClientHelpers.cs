﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MVC_4_5_Learning.Helpers
{
  public static class HttpClientHelpers
  {

    public static HttpClient CreateHttpClient()
    {
      var client = new HttpClient { BaseAddress = new Uri("http://localhost:50122") };

      client.DefaultRequestHeaders.Clear();
      client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

      return client;
    }

    public static T GetDataFromResponse<T>(HttpResponseMessage response)
    {
      var data = response.Content.ReadAsStringAsync().Result;

      return JsonConvert.DeserializeObject<T>(data);
    }


    public static async Task<T> GetHttpGetData<T>(string path)
    {
      T responseData = default(T);

      var response = await GetHttpGetResponse(path);

      if (response.IsSuccessStatusCode)
      {
        responseData = GetDataFromResponse<T>(response);
      }

      return responseData;
    }

    public static async Task<HttpResponseMessage> GetHttpGetResponse(string path)
    {
      HttpResponseMessage response;
      using (var client = CreateHttpClient())
      {
        response = await client.GetAsync(path);
      }

      return response;
    }

    public static async Task<HttpResponseMessage> GetHttpDeleteResponse(string path)
    {
      HttpResponseMessage response;
      using (var client = CreateHttpClient())
      {
        response = await client.DeleteAsync(path);
      }

      return response;
    }

    public static async Task<bool> GetHttpDeleteResult(string path)
    {
      var response = await GetHttpDeleteResponse(path);

      return response.IsSuccessStatusCode;
    }

    public static async Task<bool> GetHttpPostResult(string path, object data)
    {
      var response = await GetHttpPostResponse(path, data);

      return response.IsSuccessStatusCode;
    }

    private static async Task<HttpResponseMessage> GetHttpPostResponse(string path, object data)
    {
      HttpResponseMessage response;
      using (var client = CreateHttpClient())
      {
        response = await client.PostAsync(path, BuildJsonContent(data));
      }

      return response;
    }

    public static async Task<bool> GetHttpPutResult(string path, object data)
    {
      var response = await GetHttpPutResponse(path, data);

      return response.IsSuccessStatusCode;
    }

    private static async Task<HttpResponseMessage> GetHttpPutResponse(string path, object data)
    {
      HttpResponseMessage response;
      using (var client = CreateHttpClient())
      {
        response = await client.PutAsync(path, BuildJsonContent(data));
      }

      return response;
    }

    private static StringContent BuildJsonContent(object data)
    {
      return new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
    }
  }
}